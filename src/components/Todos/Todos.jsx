import React, { Fragment, useEffect, useState } from 'react';
import "./todos.css"

function Todos() {
    let [input,setInp] = useState("")
    let [task,setTask]=useState([])
    let [index,setindex] = useState('')
    let [isfinished,setIsFinished] = useState(false)

    function addTask(){
        let obj = {todo:"",isCompleted:false};
        obj.todo=input;
        setTask([...task,obj])
        console.log(task);
    }
    // useEffect(()=>{
    // },[isCompleted])
    function deleteTodo(e){
        
    }
    
    function newList(){
        
    }
  return (
    <Fragment>
        <div className="container">
        <div className="todosCard">
            <section className="inputSection">
                <h2>My To do List</h2>
                <div className='inpContainer'>
                    <input type="text" onChange={(e)=>{setInp(e.target.value); console.log(input) }} className='inputTag' placeholder='Title' />
                    <button onClick={()=>{addTask()}}>Add</button>
                </div>
            </section>
            <section className='outputSection'>
                
                <div className='chkbox-container'>
                <label htmlFor="all">
                <input type="checkbox"  name="taskType" id="all" value="all" />
                    All
                </label>

                <label htmlFor="Completed">
                <input type="checkbox" name="taskType" id="Completed" value="Completed" />
                    Completed
                </label>

                <label htmlFor="Incompleted">
                <input type="checkbox" name="taskType" id="Incompleted" value="Incompleted" />
                    Incompleted
                </label>
                </div>
                
                  {
                    task.map((item,index)=>{
                        return(
                    <div key={index} onClick={()=>{item.isCompleted=!item.isCompleted ; console.log(`tejas@ ${index}`);}} className='disOutput'>
                    <div className='checked-n-task' >
                    <div>
                        {
                            item.isCompleted? <input type="checkbox" checked  className='opcheckbox' />:<input type="checkbox"  className='opcheckbox'/>
                        }
                        
                    </div>
                    <p>{item.todo}</p>
                    </div>
                    <input type="reset" onClick={(e)=>{let newTaskList=[]
                                                    task.map((current,newInd)=>{
                                                    if(newInd!==index){
                                                        return(
                                                            newTaskList.push(current)   
                                                        )
                                                    }        
                                                })
        setTask([...newTaskList])}} value="X" name={index} alt="Clear the search form"/>
                </div>
                        )
                    })
                } 
                
            </section>
        </div>
        </div>
    </Fragment>
  )
}

export default Todos